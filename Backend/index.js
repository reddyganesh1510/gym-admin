import express from "express";
import "dotenv/config";
import fileUpload from "express-fileupload";
import ApiRoutes from "./routes/api.js";

const app = express();
const PORT = process.env.PORT || 8000;

// * Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static("public"));
app.use(fileUpload());

app.get("/", (req, res) => {
  return res.json({ message: "Hello It's working." });
});
app.use("/api", ApiRoutes);

app.listen(PORT, () => console.log(`Server is running on PORT ${PORT}`));
